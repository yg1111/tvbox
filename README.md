# Tvbox在线接口

#### 介绍
本PG在线配置非官方，本地包来自PG节奏熊猫发布频道，自己在线接口，请不要转发！接口不定期更新。

自用直播永久地址：https://gitee.com/linjun1001/tvbox/raw/master/tw.txt

自用点播永久地址：https://gitee.com/linjun1001/tvbox/raw/master/jsm.json  （不定时更新）

自用点播永久地址：https://gitee.com/linjun1001/tvbox/raw/master/zypg/jsm.json (稳定版)


#### 更新说明

1.  2024.4.28 优化磁力播放，本次更新具有实验性质，主要解决冷链无速度问题，因为使用了一些特殊的加速方式，所以在从磁力内容退出后，可能播放器仍在跑流量，此时只需要播放任意非磁力内容就可以解决，当然杀掉后台也行。本次更新仅兼容OK版影视播放器，其他播放器可能会报错。




#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  PG节奏熊猫发布频道  [https://t.me/PandaGroovePG](https://t.me/PandaGroovePG)
3.  我的电报频道  [https://t.me/m66996666](https://t.me/m66996666) 
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
